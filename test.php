<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Rastrotthana</title>
    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="css/modern-business.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0/css/bootstrap.css" rel="stylesheet" />
    <link href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap4.min.css" rel="stylesheet" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0/css/bootstrap.css" rel="stylesheet" />
    <link href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap4.min.css"  rel="stylesheet" />
    <link href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.bootstrap4.min.css" rel="stylesheet" />
</head>
<body>
    <!-- Navigation -->
    <nav class="navbar fixed-top navbar-expand-lg navbar-dark bg-dark fixed-top">
        <div class="container">
            <a class="navbar-brand" href="index.php">Rastrotthana Yoga Kendra</a>
                <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
            <div class="collapse navbar-collapse" id="navbarResponsive">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="index.php">Home</a>
                    </li> 
                    <li class="nav-item">
                    <a class="nav-link" href="test.php">Entries</a>
                    </li>            
                </ul>
            </div>
        </div>
    </nav>
    <div class="container">
        <div class="body table-responsive">
            <hr>
            <form class="form-horizontal" action="" method="post">
                <table id="example" class="table table-striped table-bordered table-hover js-basic-example dataTable" style="width:100%">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Age</th>
                            <th>D.O.B</th>
                            <th>Blood Group</th>
                            <th>Mobile Number</th>
                            <th>Weight</th>
                            <th>Batch No.</th>
                            <th>Edit</th>
                            <th>Delete</th>
                        </tr>
                    </thead>
                    <tbody>
                        <!-- <tr>
                            <td>Anand</td>
                            <td>27</td>
                            <td>2.8.1991</td>
                            <td>B+</td>
                            <td>9071380002</td>
                            <td>89</td>
                            <td>YS(11AM)</td>
                            <td><a href="" class="btn btn-primary">Edit</a></td>
                            <td><a href="" class="btn btn-danger">Delete</a></td>
                        </tr>
                        <tr>
                            <td>Vinay</td>
                            <td>58</td>
                            <td>4.10.1960</td>
                            <td>B-</td>
                            <td>9071380001</td>
                            <td>85</td>
                            <td>YS(9AM)</td>
                            <td><a href="" class="btn btn-primary">Edit</a></td>
                            <td><a href="" class="btn btn-danger">Delete</a></td>
                        </tr> -->
                        <?php
                            include("db-init.php");                                        
                            $sql1 = "SELECT application_id,full_name,age,dob,blood_group,mb_num,weight,batch FROM application_form";
                            $query1 = mysqli_query($conn,$sql1);                                        
                            if (!$query1) {
                                die ('SQL Error: ' . mysql_error());
                            }
                            while ($row1 = mysqli_fetch_array($query1)):
                            ?>
                            <tr>                                                
                                <th scope="row"><?= $row1['full_name']; ?></td>
                                <td><?= $row1['age']; ?></td>
                                <td><?= $row1['dob']; ?></td>
                                <td><?= $row1['blood_group']; ?></td>
                                <td><?= $row1['mb_num']; ?></td>
                                <td><?= $row1['weight']; ?></td>
                                <td><?= $row1['batch']; ?></td> 
                                <td>
                                    <form method="post" action="update.php">
                                        <input type="hidden" name="application_id" value="<?= $row1['application_id']; ?>" />
                                        <button class="btn btn-primary" type="submit" name="edit">Edit</button>
                                    </form>
                                    </td>
                                    <td>
                                    <form method="post" action="delete.php">
                                        <input type="hidden" name="application_id" value="<?= $row1['application_id']; ?>" />
                                        <button class="btn btn-danger" type="submit" name="delete">Delete</button>
                                    </form>
                                </td>  
                            </tr>
                            <?php endwhile; ?> 
                    </tbody>
                </table>
            </form>
            <hr>
        </div>
    </div>

       
       <!-- <td>
            <form method="post" action="update_cust">
                <input type="hidden" name="cust_id" value="'.$row1['cust_id'].'" />
                <button class="btn bg-pink waves-effect" type="submit" name="edit_cust"><i class="material-icons">mode_edit</i></button>
            </form>
            </td>
            <td>
            <form method="post" action="cust_function">
                <input type="hidden" name="cust_id" value="'.$row1['cust_id'].'" />
                <input type="hidden" name="status" value="inactive" />
                <button class="btn bg-pink waves-effect" type="submit" name="make_inactive"><i class="material-icons">cancel</i></button>
            </form>
        </td> -->
        

    <footer class="py-5 bg-dark">
        <div class="container">
            <p class="m-0 text-center text-white">Copyright &copy; OESPL</p>
        </div>
    </footer>
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.bootstrap4.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.colVis.min.js"></script>
    
    <script>
       $(document).ready(function() {
            var table = $('#example').DataTable( {
                lengthChange: false,
                buttons: [ 'copy', 'excel', 'pdf', 'colvis' ]
            } );
        
            table.buttons().container()
                .appendTo( '#example_wrapper .col-md-6:eq(0)' );
        } );
    </script>
  </body>
</html>
