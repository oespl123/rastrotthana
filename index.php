<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Rastrotthana</title>
    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="css/modern-business.css" rel="stylesheet">
    <SCRIPT type="text/javascript">    
          window.history.forward();
          function noBack() { 
              window.history.forward(); 
          }
    </SCRIPT>
  </head>

  <body onload="noBack();" onpageshow="if (event.persisted) noBack();" onunload="">

    <!-- Navigation -->
    <nav class="navbar fixed-top navbar-expand-lg navbar-dark bg-dark fixed-top">
      <div class="container">
        <a class="navbar-brand" href="index.php">Rastrotthana Yoga Kendra</a>
          <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a class="nav-link" href="index.php">Home</a>
            </li> 
            <li class="nav-item">
              <a class="nav-link" href="test.php">Entries</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>
    <div class="container">
      <hr>
        <form class="form-horizontal" action="index.php" method="post">
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label for="full_name" class="control-label">Name</label>
                <input type="text" class="form-control" id="full_name" name="full_name" required placeholder="Enter Your Full Name">
              </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                <label for="blood_group" class="control-label">Blood Group</label>
                  <select class="form-control" id="blood_group" name="blood_group">
                    <option value="0">Select Blood Group</option>
                    <option value="A+">A+</option>
                    <option value="A-">A-</option>
                    <option value="B+">B+</option>
                    <option value="B-">B-</option>
                    <option value="O+">O+</option>
                    <option value="O-">O-</option>
                    <option value="AB-">AB-</option>
                    <option value="AB+">AB+</option>
                  </select>
              </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                <label for="gender" class="control-label">Gender</label>
                  <select class="form-control" id="gender" name="gender">
                    <option value="0">Select Gender</option>
                    <option value="Male">Male</option>
                    <option value="Female">Female</option>
                    <option value="Others">Others</option>
                  </select>
              </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                  <label for="marital_status" class="control-label">Marital Status</label>
                  <select class="form-control" id="marital_status" name="marital_status">
                    <option value="0">Select Marital Status</option>
                    <option value="Married">Married</option>
                    <option value="Unmarried">Unmarried</option>
                  </select>
              </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                <label for="dob" class="control-label">Date Of Birth</label>  
                <input type="date" class="form-control" id="dob" name="dob"> 
              </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                <label for="age" class="control-label">Age</label>  
                <input type="number" class="form-control" id="age" name="age" placeholder="Enter Your Age"> 
              </div>
            </div>  
            <div class="col-md-3">
              <div class="form-group">
                <label for="weight" class="control-label">Weight</label>  
                <input type="text" class="form-control" id="weight" name="weight" placeholder="Enter Your Weight"> 
              </div>
            </div>  
            <div class="col-md-6">
              <div class="form-group">
                <label for="occupation" class="control-label">Occupation</label>  
                <input type="text" class="form-control" id="occupation" name="occupation" placeholder="Enter Occupation">
              </div>
            </div>  
            <div class="col-md-6">
              <div class="form-group">
                <label for="id_number" class="control-label">Identification Number</label>  
                <input type="text" class="form-control" id="id_number" name="id_number" placeholder="Enter Identification Number"> 
              </div>
            </div>  
            <div class="col-md-12">
              <div class="form-group">
                <label for="address" class="control-label">Address</label>  
                <textarea class="form-control" id="address" name="address" placeholder="Enter Address"></textarea>
              </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                <label for="landmark" class="control-label">Land Mark</label>  
                <input type="text" class="form-control" id="landmark" name="landmark" placeholder="Enter LandMark"> 
              </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                <label for="pincode" class="control-label">Pincode</label>  
                <input type="text" class="form-control" id="pincode" name="pincode" placeholder="Enter Pincode"> 
              </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                <label for="mb_num" class="control-label">Mobile Number</label>  
                <input type="number" class="form-control" id="mb_num" name="mb_num" placeholder="Enter your Mobile Number"> 
              </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                <label for="email" class="control-label">E-Mail</label>  
                <input type="email" class="form-control" min="10" max="10" id="email" name="email" placeholder="Enter your E-Mail-id"> 
              </div>
            </div>
            <div class="col-md-12">
              <div class="form-group">
                <label class="control-label">Why do you want to join our YOGA classes?</label>
                  <div class="form-check">
                    <input type="checkbox" class="form-check-input" id="treat" name="why_yoga[]" value="For treatment through yoga therapy.">
                    <label class="form-check-label" for="treat">For treatment through yoga therapy.</label>
                  </div>
                <div class="form-check">
                    <input type="checkbox" class="form-check-input" id="health" name="why_yoga[]" value="Health Precautions">
                    <label class="form-check-label" for="health">Health Precautions.</label>
                </div>
                <div class="form-check">
                    <input type="checkbox" class="form-check-input" id="learn" name="why_yoga[]" value="To learn yoga and to spread yoga">
                    <label class="form-check-label" for="learn">To learn yoga and to spread yoga.</label>
                </div>
                <div class="form-check">
                    <input type="checkbox" class="form-check-input" id="just" name="why_yoga[]" value="Just for Curiosity">
                    <label class="form-check-label" for="just">Just for Curiosity.</label>
                </div>
                <div class="form-check">
                    <input type="checkbox" class="form-check-input" id="reason" name="why_yoga[]" value="Any other Reason">
                    <label class="form-check-label" for="reason">Any other Reason.</label>
                </div>
              </div>
            </div>
            <div class="col-md-12">
              <h3>Personal Details</h3>
            </div>
            <div class="col-md-4"> 
              <label for="appetite" class="control-label">Normal Appetite</label>
                <select class="form-control" id="appetite" name="appetite">
                  <option value="Yes">Yes</option>
                  <option value="No">No</option>
                </select><br>
                <div id="info_appetite">
                    <input type="text" id="appetite_info" name="appetite_info" class="form-control" placeholder="If 'NO', please explain" value="" />
                </div>
            </div>
            <div class="col-md-4">
              <label for="sleep" class="control-label">Normal Sleep</label>
                <select class="form-control" id="sleep" name="sleep">
                  <option value="Yes">Yes</option>
                  <option value="No">No</option>
                </select><br>
                <div id="info_sleep">
                    <input type="text" id="sleep_info" name="sleep_info" class="form-control" placeholder="If 'NO', please explain" value="" />
                </div>

            </div>
            <div class="col-md-4">
                <label for="bowels" class="control-label">Normal Bowels</label>
                  <select class="form-control" id="bowels" name="bowels">
                    <option value="Yes">Yes</option>
                    <option value="No">No</option>
                  </select><br>
                  <div id="info_bowels">
                    <input type="text" id="bowels_info" name="bowels_info" class="form-control" placeholder="If 'NO', please explain" value="" />
                </div>
            </div>
            <div class="col-md-12">
              <h3>For Female Students Only</h3>
            </div>
            <div class="col-md-3"> 
              <label for="cycle" class="control-label">Menstrual Cycle</label>
                <select class="form-control" id="cycle" name="cycle">
                  <option value="NA">NA</option>
                  <option value="Regular">Regular</option>
                  <option value="Irregular">Irregular</option>
                </select>
            </div>
            <div class="col-md-3"> 
              <label for="status" class="control-label">If Pregnant, status</label>
                <select class="form-control" id="status" name="status">
                  <option value="NA">NA</option>
                  <option value="No">No</option>
                  <option value="Yes">Yes</option>
                </select><br>
                <div id="info_status">
                    <input type="text" id="status_info" name="status_info" class="form-control" value="" />
                </div>
            </div>
            <div class="col-md-3"> 
              <label for="disorder" class="control-label">Menstrual disorders</label>
                <select class="form-control" id="disorder" name="disorder">
                  <option value="NA">NA</option>
                  <option value="No">No</option>
                  <option value="Yes">Yes</option>
                </select><br>
                <div id="info_disorder">
                    <input type="text" id="disorder_info" name="disorder_info" class="form-control" value="" />
                </div>
            </div>
            <div class="col-md-3"> 
              <label for="problems" class="control-label">Any other gynecologic problems</label>
                <select class="form-control" id="problems" name="problems">
                  <option value="NA">NA</option>
                  <option value="No">No</option>
                  <option value="Yes">Yes</option>
                </select><br>
                  <div id="info_gynecologic">
                    <input type="text" id="gynecologic_info" name="gynecologic_info" class="form-control" value="" />
                  </div>
            </div>
            <hr>
            <div class="col-md-12">
              <div class="form-group">
                <label for="diseases" class="control-label">Diseases Information</label>  
                <textarea class="form-control" id="diseases" name="diseases" placeholder="Example:Bach Ache, Heart Problems,Stress, Diabetes, Migraine, etc..,"></textarea>
              </div>
            </div>
            <div class="col-md-12">
              <div class="form-group">
                <label for="hobbies" class="control-label">Hobbies and Intrests</label>  
                  <input type="text" class="form-control" id="hobbies" name="hobbies" placeholder="Music, Sports, Story Writing, Social Service etc..,">
              </div>
            </div>
            <div class="col-md-6"> 
                <label for="membership" class="control-label">MEMBERSHIP IN ANY ORGANIZATION</label>
                  <select class="form-control" id="membership" name="membership">
                    <option value="No">No</option>
                    <option value="Yes">Yes</option>
                </select><br>
                <div id="info_membership">
                    <input type="text" id="membership_info" name="membership_info" class="form-control" placeholder="If 'YES', please mention" value="" />
                </div>
            </div>
            <div class="col-md-6"> 
              <label for="institution" class="control-label">How did you come to know about this institution</label>
                <select class="form-control" id="intitution" name="institution">
                  <option value="0">Select the source</option>
                  <option value="Friends/Relativies">Friends/Relativies</option>
                  <option value="News Paper">News Paper</option>
                  <option value="Sign Boards">Sign Boards</option>
                  <option value="Pamphlets">Pamphlets</option>
                </select>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label for="date" class="control-label">Date</label>  
                <input type="date" class="form-control" id="date" name="date"> 
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label for="place" class="control-label">Place</label>  
                  <input type="text" class="form-control" id="place" name="place" placeholder="Enter Place"> 
              </div>
            </div>
            <div class="col-md-4"> 
              <label for="branch" class="control-label">Branch</label>
                <select class="form-control" id="branch" name="branch">
                  <option value="0">Select your Branch</option>
                  <option value="Jayanagara">Jayanagara</option>
                  <option value="Sadashivanagar">Sadashivanagar</option>
                  <option value="Keshava Shilpa">Keshava Shilpa</option>
                  <option value="Sunkena Halli">Sunkena Halli</option>
                  <option value="Kalyan Nagar">Kalyan Nagar</option>
                  <option value="Kundala Halli">Kundala Halli</option>
                </select>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label for="batch" class="control-label">Batch No.</label>  
                <input type="text" class="form-control" id="batch" name="batch" placeholder="Enter Batch Number"> 
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label for="id" class="control-label">ID No.</label>  
                <input type="text" class="form-control" id="id" name="id"> 
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label for="receipt" class="control-label">Receipt No.</label>  
                <input type="text" class="form-control" id="receipt" name="receipt"> 
              </div>
            </div>
            <div class="col-md-12 text-center">
                <button type="submit" class="btn btn-primary btn-lg" name="application_form">Submit</button>
            </div>
          </div>
        </div>
      </form>
      <hr>
    </div>
    <footer class="py-5 bg-dark">
      <div class="container">
        <p class="m-0 text-center text-white">Copyright &copy; OESPL</p>
      </div>
    </footer>
    <?php include('application_form.php'); ?>
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script>
      $(document).ready(function () {
          $("#info_appetite").hide();
          $("#info_sleep").hide();
          $("#info_bowels").hide();
          $("#info_status").hide();
          $("#info_disorder").hide();
          $("#info_gynecologic").hide();
          $("#info_membership").hide();
          $('form').each(function() { this.reset() });          
      });

      $("#appetite").change(function(){
        $('#info_appetite').show();
      });
      $("#sleep").change(function(){
        $('#info_sleep').show();
      });
      $("#bowels").change(function(){
        $('#info_bowels').show();
      });
      $("#status").change(function(){
        $('#info_status').show();
      });
      $("#disorder").change(function(){
        $('#info_disorder').show();
      });
      $("#problems").change(function(){
        $('#info_gynecologic').show();
      });
      $("#membership").change(function(){
        $('#info_membership').show();
      });
    </script>
  </body>
</html>
