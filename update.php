<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Rastrotthana</title>
    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="css/modern-business.css" rel="stylesheet">

  </head>

  <body>

    <!-- Navigation -->
    <nav class="navbar fixed-top navbar-expand-lg navbar-dark bg-dark fixed-top">
      <div class="container">
        <a class="navbar-brand" href="index.php">Rastrotthana Yoga Kendra</a>
          <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <!-- <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a class="nav-link" href="index.php">Home</a>
            </li> 
            <li class="nav-item">
              <a class="nav-link" href="test.php">Entries</a>
            </li>
          </ul> -->
        </div>
      </div>
    </nav>
    <div class="container">
      <hr>
        <form class="form-horizontal" action="update.php" method="post">
            <?php 
                include("db-init.php"); 
                // $var_value = $_POST['application_id']; 
                $var_value = $_POST['application_id'];
                $sql_product_info = "SELECT * FROM application_form where application_id='$var_value';";
                $query_product_info = $conn->query($sql_product_info);
                while ($product_info = mysqli_fetch_array($query_product_info)){
                        //     $p_name = $product_info['product_name']; 
                        //     $partnumber = $product_info['part_number']; 
                        //     $hsncode = $product_info['hsn_code'];  
                        //     $p_description = $product_info['product_description']; 

                    $full_name = $product_info['full_name'];
                    $blood_group = $product_info['blood_group'];
                    $gender = $product_info['gender'];
                    $marital_status = $product_info['marital_status'];
                    $dob = $product_info['dob'];
                    $age = $product_info['age'];
                    $weight = $product_info['weight'];
                    $occupation = $product_info['occupation'];
                    $id_number = $product_info['id_number'];
                    $address = $product_info['address'];
                    $landmark = $product_info['landmark'];
                    $pincode = $product_info['pincode'];
                    $mb_num = $product_info['mb_num'];
                    $email = $product_info['email'];
                    $appetite = $product_info['appetite'];
                    $appetite_info = $product_info['appetite_info'];
                    $sleep = $product_info['sleep'];
                    $sleep_info = $product_info['sleep_info'];
                    $bowels = $product_info['bowels'];
                    $bowels_info = $product_info['bowels_info'];
                    $cycle = $product_info['cycle'];
                    $status = $product_info['status'];
                    $disorder = $product_info['disorder'];
                    $disorder_info = $product_info['disorder_info'];
                    $problems = $product_info['problems'];
                    $gynecologic_info = $product_info['gynecologic_info'];
                    $diseases = $product_info['diseases'];
                    $hobbies = $product_info['hobbies'];
                    $membership = $product_info['membership'];
                    $membership_info = $product_info['membership_info'];
                    $institution = $product_info['institution'];
                    $date = $product_info['date'];
                    $place = $product_info['place'];
                    $branch = $product_info['branch'];
                    $batch = $product_info['batch'];  
                    $id = $product_info['id'];
                    $receipt = $product_info['receipt']; 
                }
            ?>
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label for="full_name" class="control-label">Name</label>
                <input type="text" class="form-control" id="full_name" name="full_name" required placeholder="Enter Name" value="<?php echo $full_name; ?>">
              <input type="hidden" id="application_id" name="application_id" value="<?= $var_value; ?>" />
              </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                <label for="blood_group" class="control-label">Blood Group</label>
                  <select class="form-control" id="blood_group" name="blood_group">
                    <option value="0"><?php echo $blood_group; ?></option>
                    <option value="A+">A+</option>
                    <option value="A-">A-</option>
                    <option value="B+">B+</option>
                    <option value="B-">B-</option>
                    <option value="O+">O+</option>
                    <option value="O-">O-</option>
                    <option value="AB-">AB-</option>
                    <option value="AB+">AB+</option>
                  </select>
              </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                <label for="gender" class="control-label">Gender</label>
                  <select class="form-control" id="gender" name="gender">
                    <option value="0"><?php echo $gender; ?></option>
                    <option value="Male">Male</option>
                    <option value="Female">Female</option>
                    <option value="Others">Others</option>
                  </select>
              </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                  <label for="marital_status" class="control-label">Marital Status</label>
                  <select class="form-control" id="marital_status" name="marital_status">
                    <option value="0"><?php echo $marital_status; ?></option>
                    <option value="Married">Married</option>
                    <option value="Unmarried">Unmarried</option>
                  </select>
              </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                <label for="dob" class="control-label">Date Of Birth</label>  
                <input type="date" class="form-control" id="dob" name="dob" placeholder="Enter Date Of Birth" value="<?php echo $dob; ?>"> 
              </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                <label for="age" class="control-label">Age</label>  
                <input type="number" class="form-control" id="age" name="age" placeholder="Enter Your Age" value="<?php echo $age; ?>"> 
              </div>
            </div>  
            <div class="col-md-3">
              <div class="form-group">
                <label for="weight" class="control-label">Weight</label>  
                <input type="text" class="form-control" id="weight" name="weight" placeholder="Enter Your Weight" value="<?php echo $weight; ?>"> 
              </div>
            </div>  
            <div class="col-md-6">
              <div class="form-group">
                <label for="occupation" class="control-label">Occupation</label>  
                <input type="text" class="form-control" id="occupation" name="occupation" placeholder="Enter Occupation" value="<?php echo $occupation; ?>">
              </div>
            </div>  
            <div class="col-md-6">
              <div class="form-group">
                <label for="id_number" class="control-label">Identification Number</label>  
                <input type="text" class="form-control" id="id_number" name="id_number" placeholder="Enter ID Number" value="<?php echo $id_number; ?>"> 
              </div>
            </div>  
            <div class="col-md-12">
              <div class="form-group">
                <label for="address" class="control-label">Address</label>  
                <input type="text" class="form-control" id="address" name="address" placeholder="Enter Your Address" value="<?php echo $address; ?>">
              </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                <label for="landmark" class="control-label">Land Mark</label>  
                <input type="text" class="form-control" id="landmark" name="landmark" placeholder="Enter Landmark" value="<?php echo $landmark; ?>"> 
              </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                <label for="pincode" class="control-label">Pincode</label>  
                <input type="text" class="form-control" id="pincode" name="pincode" placeholder="Enter PINCODE" value="<?php echo $pincode; ?>"> 
              </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                <label for="mb_num" class="control-label">Mobile Number</label>  
                <input type="number" class="form-control" id="mb_num" name="mb_num" placeholder="Enter your Mobile Number" value="<?php echo $mb_num; ?>"> 
              </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                <label for="email" class="control-label">E-Mail</label>  
                <input type="email" class="form-control" min="10" max="10" id="email" name="email" placeholder="Enter your E-Mail-id" value="<?php echo $email; ?>"> 
              </div>
            </div>
            <div class="col-md-12">
              <div class="form-group">
                <label class="control-label">Why do you want to join our YOGA classes?</label>
                  <div class="form-check">
                    <input type="checkbox" class="form-check-input" id="treat" name="why_yoga[]" value="For treatment through yoga therapy.">
                    <label class="form-check-label" for="treat">For treatment through yoga therapy.</label>
                  </div>
                <div class="form-check">
                    <input type="checkbox" class="form-check-input" id="health" name="why_yoga[]" value="Health Precautions">
                    <label class="form-check-label" for="health">Health Precautions.</label>
                </div>
                <div class="form-check">
                    <input type="checkbox" class="form-check-input" id="learn" name="why_yoga[]" value="To learn yoga and to spread yoga">
                    <label class="form-check-label" for="learn">To learn yoga and to spread yoga.</label>
                </div>
                <div class="form-check">
                    <input type="checkbox" class="form-check-input" id="just" name="why_yoga[]" value="Just for Curiosity">
                    <label class="form-check-label" for="just">Just for Curiosity.</label>
                </div>
                <div class="form-check">
                    <input type="checkbox" class="form-check-input" id="reason" name="why_yoga[]" value="Any other Reason">
                    <label class="form-check-label" for="reason">Any other Reason.</label>
                </div>
              </div>
            </div>
            <div class="col-md-12">
              <h3>Personal Details</h3>
            </div>
            <div class="col-md-4"> 
              <label for="appetite" class="control-label">Normal Appetite</label>
                <select class="form-control" id="appetite" name="appetite">
                  <option value><?php echo $appetite; ?></option>
                  <option value="Yes">Yes</option>
                  <option value="No">No</option>
                </select><br>
                <div id="info_appetite">
                    <input type="text" id="appetite_info" name="appetite_info" class="form-control" placeholder="If 'NO', please explain" value="<?php echo $appetite_info; ?>" />
                </div>
            </div>
            <div class="col-md-4">
              <label for="sleep" class="control-label">Normal Sleep</label>
                <select class="form-control" id="sleep" name="sleep">
                  <option value><?php echo $sleep; ?></option>
                  <option value="Yes">Yes</option>
                  <option value="No">No</option>
                </select><br>
                <div id="info_sleep">
                    <input type="text" id="sleep_info" name="sleep_info" class="form-control" placeholder="If 'NO', please explain" value="<?php echo $sleep_info; ?>" />
                </div>

            </div>
            <div class="col-md-4">
                <label for="bowels" class="control-label">Normal Bowels</label>
                  <select class="form-control" id="bowels" name="bowels">
                    <option value><?php echo $bowels; ?></option>
                    <option value="Yes">Yes</option>
                    <option value="No">No</option>
                  </select><br>
                  <div id="info_bowels">
                    <input type="text" id="bowels_info" name="bowels_info" class="form-control" placeholder="If 'NO', please explain" value="<?php echo $bowels_info; ?>" />
                </div>
            </div>
            <div class="col-md-12">
              <h3>For Female Students Only</h3>
            </div>
            <div class="col-md-3"> 
              <label for="cycle" class="control-label">Menstrual Cycle</label>
                <select class="form-control" id="cycle" name="cycle">
                  <option value><?php echo $cycle; ?></option>
                  <option value="NA">NA</option>
                  <option value="Regular">Regular</option>
                  <option value="Irregular">Irregular</option>
                </select>
            </div>
            <div class="col-md-3"> 
              <label for="status" class="control-label">If Pregnant, status</label>
                <select class="form-control" id="status" name="status">
                  <option value><?php echo $status; ?></option>
                  <option value="NA">NA</option>
                  <option value="No">No</option>
                  <option value="Yes">Yes</option>
                </select><br>
                <div id="info_status">
                    <input type="text" id="status_info" name="status_info" class="form-control" value="" />
                </div>
            </div>
            <div class="col-md-3"> 
              <label for="disorder" class="control-label">Menstrual disorders</label>
                <select class="form-control" id="disorder" name="disorder">
                  <option value><?php echo $disorder; ?></option>
                  <option value="NA">NA</option>
                  <option value="No">No</option>
                  <option value="Yes">Yes</option>
                </select><br>
                <div id="info_disorder">
                    <input type="text" id="disorder_info" name="disorder_info" class="form-control" value="<?php echo $disorder_info; ?>" />
                </div>
            </div>
            <div class="col-md-3"> 
              <label for="problems" class="control-label">Any other gynecologic problems</label>
                <select class="form-control" id="problems" name="problems">
                  <option value><?php echo $problems; ?></option>
                  <option value="NA">NA</option>
                  <option value="No">No</option>
                  <option value="Yes">Yes</option>
                </select><br>
                  <div id="info_gynecologic">
                    <input type="text" id="gynecologic_info" name="gynecologic_info" class="form-control" value="<?php echo $gynecologic_info; ?>" />
                  </div>
            </div>
            <hr>
            <div class="col-md-12">
              <div class="form-group">
                <label for="diseases" class="control-label">Diseases Information</label>  
                <input type="text" class="form-control" id="diseases" name="diseases" placeholder="Example:Bach Ache, Heart Problems,Stress, Diabetes, Migraine, etc..," value="<?php echo $diseases; ?>">
              </div>
            </div>
            <div class="col-md-12">
              <div class="form-group">
                <label for="hobbies" class="control-label">Hobbies and Intrests</label>  
                  <input type="text" class="form-control" id="hobbies" name="hobbies" placeholder="Music, Sports, Story Writing, Social Service etc..," value="<?php echo $hobbies; ?>">
              </div>
            </div>
            <div class="col-md-6"> 
                <label for="membership" class="control-label">MEMBERSHIP IN ANY ORGANIZATION</label>
                  <select class="form-control" id="membership" name="membership">
                    <option value><?php echo $membership; ?></option>  
                    <option value="No">No</option>
                    <option value="Yes">Yes</option>
                </select><br>
                <div id="info_membership">
                    <input type="text" id="membership_info" name="membership_info" class="form-control" placeholder="If 'YES', please mention" value="<?php echo $membership_info; ?>" />
                </div>
            </div>
            <div class="col-md-6"> 
              <label for="institution" class="control-label">How did you come to know about this institution</label>
                <select class="form-control" id="intitution" name="institution">
                  <option value><?php echo $institution; ?></option>
                  <option value="0">Select the source</option>
                  <option value="Friends/Relativies">Friends/Relativies</option>
                  <option value="News Paper">News Paper</option>
                  <option value="Sign Boards">Sign Boards</option>
                  <option value="Pamphlets">Pamphlets</option>
                </select>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label for="date" class="control-label">Date</label>  
                <input type="date" class="form-control" id="date" name="date" value="<?php echo $date; ?>"></option>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label for="place" class="control-label">Place</label>  
                  <input type="text" class="form-control" id="place" name="place" placeholder="Enter Place" value="<?php echo $place; ?>"> 
              </div>
            </div>
            <div class="col-md-4"> 
              <label for="branch" class="control-label">Branch</label>
                <select class="form-control" id="branch" name="branch">
                  <option value="0"><?php echo $branch; ?></option>
                  <option value="Jayanagara">Jayanagara</option>
                  <option value="Sadashivanagar">Sadashivanagar</option>
                  <option value="Chamrajpet">Chamrajpet</option>
                </select>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label for="batch" class="control-label">Batch No.</label>  
                <input type="text" class="form-control" id="batch" name="batch" placeholder="Enter Batch Number" value="<?php echo $batch; ?>"> 
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label for="id" class="control-label">ID No.</label>  
                <input type="text" class="form-control" id="id" name="id" value="<?php echo $id; ?>"> 
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label for="receipt" class="control-label">Receipt No.</label>  
                <input type="text" class="form-control" id="receipt" name="receipt" value="<?php echo $receipt; ?>"> 
              </div>
            </div>
            <div class="col-md-12 text-center">
                <button type="submit" class="btn btn-primary btn-lg" name="application_form">UPDATE</button>
                <button type="button" class="btn btn-primary btn-lg" id="cancel" name="cancel">CANCEL</button>
            </div>
          </div>
        </form>
      <hr>
    </div>
    <?php 
      include('db-init.php');
      if(isset($_POST['application_form'])){
          $full_name = $_POST['full_name'];
          $blood_group = $_POST['blood_group'];
          $gender = $_POST['gender'];
          $marital_status = $_POST['marital_status'];
          $dob = $_POST['dob'];
          $age = $_POST['age'];
          $weight = $_POST['weight'];
          $occupation = $_POST['occupation'];
          $id_number = $_POST['id_number'];
          $address = $_POST['address'];
          $landmark = $_POST['landmark'];
          $pincode = $_POST['pincode'];
          $mb_num = $_POST['mb_num'];
          $email = $_POST['email'];
          $why_yoga_list = array();
          if($why_yoga_list==''){
            $why_yoga_list = 0;
          } else {
            foreach($_POST['why_yoga'] as $whyyoga){
                $why_yoga_list[] = $whyyoga;
            }   
          } 
          $why_yoga_list = implode(',', $why_yoga_list);
          $appetite = $_POST['appetite'];
          $appetite_info = $_POST['appetite_info'];
          $sleep = $_POST['sleep'];
          $sleep_info = $_POST['sleep_info'];
          $bowels = $_POST['bowels'];
          $bowels_info = $_POST['bowels_info'];
          $cycle = $_POST['cycle'];
          $status = $_POST['status'];
          $disorder = $_POST['disorder'];
          $disorder_info = $_POST['disorder_info'];
          $problems = $_POST['problems'];
          $gynecologic_info = $_POST['gynecologic_info'];
          $diseases = $_POST['diseases'];
          $hobbies = $_POST['hobbies'];
          $membership = $_POST['membership'];
          $membership_info = $_POST['membership_info'];
          $institution = $_POST['institution'];
          $date = $_POST['date'];
          $place = $_POST['place'];
          $branch = $_POST['branch'];
          $batch = $_POST['batch'];  
          $id = $_POST['id'];
          $receipt = $_POST['receipt'];  
          $application_id = $_POST['application_id'];
          $sql = "UPDATE application_form SET
          full_name='$full_name',
           blood_group='$blood_group',
            gender='$gender',
             marital_status='$marital_status', 
          dob='$dob',
           age='$age',
          weight='$weight',
           occupation='$occupation',
            id_number='$id_number',
          address='$address',
           landmark='$landmark',
            pincode='$pincode',
             mb_num='$mb_num',
              email='$email', 
          why_yoga='$why_yoga_list',
           appetite='$appetite',
            appetite_info='$appetite_info', 
            sleep='$sleep',
          sleep_info='$sleep_info',
           bowels='$bowels',
            bowels_info='$bowels_info',
             cycle='$cycle',
              status='$status', 
          disorder='$disorder',
           disorder_info='$disorder_info',
            problems='$problems',
             gynecologic_info='$gynecologic_info', 
          diseases='$diseases',
           hobbies='$hobbies',
            membership='$membership',
             membership_info='$membership_info', 
          institution='$institution',
           date='$date',
            place='$place',
             branch='$branch',
              batch='$batch',
               id='$id',
                receipt='$receipt'
          WHERE application_id='$application_id';";    
          if ($conn->query($sql) === TRUE) {
              echo "<Script>alert('Record updated successfully');
              window.location.href = 'index.php';</script>";
          } else {
              echo "<Script>alert('Error: '.$sql.'<br>');</script>".$conn->error;
          }
          // echo $sql;
        }
    ?>
    <footer class="py-5 bg-dark">
      <div class="container">
        <p class="m-0 text-center text-white">Copyright &copy; OESPL</p>
      </div>
    </footer>
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script>
      $(document).ready(function () {
          $("#info_appetite").hide();
          $("#info_sleep").hide();
          $("#info_bowels").hide();
          $("#info_status").hide();
          $("#info_disorder").hide();
          $("#info_gynecologic").hide();
          $("#info_membership").hide();
          $('form').each(function() { this.reset() }); 
      });

      $("#appetite").change(function(){
        $('#info_appetite').show();
      });
      $("#sleep").change(function(){
        $('#info_sleep').show();
      });
      $("#bowels").change(function(){
        $('#info_bowels').show();
      });
      $("#status").change(function(){
        $('#info_status').show();
      });
      $("#disorder").change(function(){
        $('#info_disorder').show();
      });
      $("#problems").change(function(){
        $('#info_gynecologic').show();
      });
      $("#membership").change(function(){
        $('#info_membership').show();
      });
      $("#cancel").click(function(){
          location.href = "test.php"
      });
    </script>
  </body>
</html>
